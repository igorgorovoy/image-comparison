package ua.horovyi.image.comparing;


import ua.horovyi.image.comparing.image.group.Groups;
import ua.horovyi.image.comparing.image.io.ImageReader;
import ua.horovyi.image.comparing.image.io.ImageWriter;
import ua.horovyi.image.comparing.image.service.ImageComparisonService;
import ua.horovyi.image.comparing.image.service.ShowImageService;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

/**
 * @author Ihor_Horovyi
 */
public class Demo {

    private static final int DEFAULT_RGB_DIFFERENCE = 10;

    private static final String DEFAULT_FIRST_IMAGE = Demo.class.getClass().getResource("/default-images/1.png").getPath();
    private static final String DEFAULT_SECOND_IMAGE = Demo.class.getClass().getResource("/default-images/2.png").getPath();
    private static final String DEFAULT_RESULT_IMAGE_NAME = "result-image";


    public static void main(String[] args) throws IOException {
        final String firstImagePath;
        final String secondImagePath;

        if (args != null && args.length == 2) {
            firstImagePath = args[0];
            secondImagePath = args[1];
        } else {
            firstImagePath = DEFAULT_FIRST_IMAGE;
            secondImagePath = DEFAULT_SECOND_IMAGE;
        }

        BufferedImage firstImage = ImageReader.read(firstImagePath);
        BufferedImage secondImage = ImageReader.read(secondImagePath);

        ImageComparisonService imageComparisonService = new ImageComparisonService(DEFAULT_RGB_DIFFERENCE);

        Groups groups = imageComparisonService.comparisonImages(firstImage, secondImage);

        paintGroups(groups, secondImage);

        // save image
        ImageWriter.write(secondImage, DEFAULT_RESULT_IMAGE_NAME);
        // show image
        ShowImageService.showImage(new ImageIcon(secondImage));
    }

    private static void paintGroups(Groups groups, BufferedImage image) {
        List<Groups.Group> pointGroup = groups.getGroups();
        Graphics2D graphics = image.createGraphics();

        graphics.setPaint(Color.red);

        pointGroup.forEach(g -> {
            Point start = g.getMin();
            Point end = g.getMax();
            graphics.drawRect(start.x, start.y, end.x - start.x, end.y - start.y);
        });
    }

}
