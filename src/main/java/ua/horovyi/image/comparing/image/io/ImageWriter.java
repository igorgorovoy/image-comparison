package ua.horovyi.image.comparing.image.io;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.MessageFormat;

/**
 * @author Ihor_Horovyi
 */
public class ImageWriter {

    private static final String EXTENSION_PNG = "PNG";
    private static final String DEFAULT_PATH_NAME = "./{0}." + EXTENSION_PNG;
    private static final String ERROR_MASSAGE = "Sorry, some problem with save image. Image: {0}, image name: {1}";

    public static void write(BufferedImage image, String imageName) {
        String pathName = MessageFormat.format(DEFAULT_PATH_NAME, imageName);
        try {
            ImageIO.write(image, EXTENSION_PNG, new File(pathName));
        } catch (Exception e) {
            throw new IllegalArgumentException(MessageFormat.format(ERROR_MASSAGE, image, imageName), e);
        }
        System.out.println("Your image saved in " + pathName);
    }
}
