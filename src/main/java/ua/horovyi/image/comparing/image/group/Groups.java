package ua.horovyi.image.comparing.image.group;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ihor_Horovyi
 */
public class Groups {

    private final int maxDistanceBetweenGroup;

    public Groups(int maxDistanceBetweenGroup) {
        this.maxDistanceBetweenGroup = maxDistanceBetweenGroup;
    }

    private List<Group> groups = new ArrayList<>();

    public List<Group> getGroups() {
        return groups;
    }

    public void putPoint(Point point) {
        for (Group group : groups) {
            if (putWhenBelongsGroup(point, group)) {
                return;
            }
        }

        Group group = new Group();
        group.setMin(point);
        groups.add(group);
    }

    private boolean putWhenBelongsGroup(Point point, Group group) {
        boolean isPointBelongsMinGroup = false;
        boolean isPointBelongsMaxGroup = false;

        if (group.getMin() != null) {
            isPointBelongsMinGroup = pointDistance(point, group.getMin()) < maxDistanceBetweenGroup;
        }

        if (comparePointLocations(group.getMin(), point)) {
            group.setMin(point);
            return true;
        }

        if (group.getMax() != null) {
            isPointBelongsMaxGroup = pointDistance(point, group.getMax()) < maxDistanceBetweenGroup;
        }

        if (isPointBelongsMinGroup && group.getMax() == null || isPointBelongsMaxGroup && comparePointLocations(point, group.getMax())) {
            group.setMax(point);
            return true;
        }

        return isPointBelongsMinGroup || isPointBelongsMaxGroup;
    }

    private static int pointDistance(Point point1, Point point2) {
        double distance = Math.sqrt(
                Math.pow(pointLocation(point1), 2) - Math.pow(pointLocation(point2), 2)
        );
        return (int) Math.abs(distance);
    }

    private static int pointLocation(Point point) {
        return point.x + point.y;
    }

    private static boolean comparePointLocations(Point point1, Point point2) {
        return pointLocation(point1) > pointLocation(point2);
    }

    public class Group {

        private Point min;
        private Point max;

        public Point getMin() {
            return min;
        }

        public void setMin(Point min) {
            this.min = min;
        }

        public Point getMax() {
            return max;
        }

        public void setMax(Point max) {
            this.max = max;
        }
    }
}
