package ua.horovyi.image.comparing.image.service;

import ua.horovyi.image.comparing.image.group.Groups;

import java.awt.*;
import java.awt.image.BufferedImage;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * @author Ihor_Horovyi
 */
public class ImageComparisonService {

    private static final int DEFAULT_MAX_DISTANCE_BETWEEN_GROUP = 100;

    private final int rgbDifference;

    public ImageComparisonService(int rgbDifference) {
        this.rgbDifference = rgbDifference;
    }

    public Groups comparisonImages(BufferedImage firstImage, BufferedImage secondImage) {
        checkImagesSize(firstImage, secondImage);

        Groups pointGroups = new Groups(DEFAULT_MAX_DISTANCE_BETWEEN_GROUP);

        int width = firstImage.getWidth();
        int height = firstImage.getHeight();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (getColourDistance(firstImage.getRGB(x, y), secondImage.getRGB(x, y)) > rgbDifference) {
                    pointGroups.putPoint(new Point(x, y));
                }
            }
        }

        return pointGroups;
    }

    private int getColourDistance(int firstImageRGB, int secondImageRGB) {
        Color firstColor = new Color(firstImageRGB);
        Color secondColor = new Color(secondImageRGB);

        double colourDistance = sqrt(
                pow(firstColor.getRed() - secondColor.getRed(), 2) *
                        pow(firstColor.getGreen() - secondColor.getGreen(), 2) *
                        pow(firstColor.getBlue() - secondColor.getBlue(), 2)
        );

        return (int) colourDistance;
    }

    public static void checkImagesSize(BufferedImage firstImage, BufferedImage secondImage) {
        if (firstImage == null && secondImage == null) {
            throw new IllegalArgumentException("Some image equals null");
        }

        if (firstImage.getWidth() != secondImage.getWidth()) {
            throw new IllegalArgumentException("Images width is different: [" + firstImage.getWidth() + "," + secondImage.getWidth() + "]");
        }

        if (firstImage.getHeight() != secondImage.getHeight()) {
            throw new IllegalArgumentException("Images height is different: [" + firstImage.getHeight() + "," + secondImage.getHeight() + "]");
        }
    }
}
