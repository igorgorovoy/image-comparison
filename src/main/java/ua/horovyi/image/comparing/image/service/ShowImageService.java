package ua.horovyi.image.comparing.image.service;

import javax.swing.*;
import java.awt.*;

/**
 * @author Ihor_Horovyi
 */
public class ShowImageService {

    public static void showImage(ImageIcon image) {
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setSize(image.getIconWidth(), image.getIconHeight());
        frame.setTitle("Result image");
        JLabel lbl = new JLabel();
        lbl.setIcon(image);
        frame.add(lbl);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
