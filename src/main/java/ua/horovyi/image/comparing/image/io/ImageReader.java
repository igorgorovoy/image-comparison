package ua.horovyi.image.comparing.image.io;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author Ihor_Horovyi
 */
public class ImageReader {

    public static BufferedImage read(String path) throws IOException {

        File resource = new File(path);

        if (!resource.exists()) {
            throw new IllegalArgumentException("Filed path to resource, path:" + path);
        }

        return javax.imageio.ImageIO.read(resource);
    }
}
